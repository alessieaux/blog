---
title: Maandag 4 september 2017:
date: 2017-09-04
tags: ["blog, "blogging", "blogboek", "new", "post", "1c"]
---
- Introductie van de project docent gekregen 
- Een teamcaptain aangewezen en algemene afspraken gemaakt 
- Een logo ontworpen 
- Een mindmap gemaakt van Rotterdam
- Alle punten voor de opdracht van vrijdag 15 september doorgelopen 
- Begonnen met het onderzoek voor de verschillende thema’s en doelgroepen
